﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }

        public VehicleType VehicleType { get; }

        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!IsIdValid(id) || balance < 0)
            {
                throw new ArgumentException();
            }
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        private bool IsIdValid(string id)
        {
            Regex regex = new Regex(@"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}");

            return regex.IsMatch(id); 
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var rng = new Random();
            var id = 
                ((char)rng.Next('A', 'Z' + 1)).ToString() +
                (char)rng.Next('A', 'Z' + 1) +
                "-" +
                rng.Next(0, 10).ToString() +
                rng.Next(0, 10).ToString() +
                rng.Next(0, 10).ToString() +
                rng.Next(0, 10).ToString() +
                "-" +
                (char)rng.Next('A', 'Z' + 1) +
                (char)rng.Next('A', 'Z' + 1);

            return id;
        }
    }
}