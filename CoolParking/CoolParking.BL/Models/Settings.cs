﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal Balance { get; private set; } = 0;

        public static int Capacity { get; private set; } = 10;

        public static double PaymentTime { get; private set; } = 5;

        public static double LogTime { get; private set; } = 60;

        public static Dictionary<VehicleType, decimal> Tariff { get; private set; } = new Dictionary<VehicleType, decimal>() { { VehicleType.Bus, 3.5m }, { VehicleType.PassengerCar, 2 }, { VehicleType.Motorcycle, 1 }, { VehicleType.Truck, 5} };

        public static decimal Penalty { get; private set; } = 2.5m;
    }
}