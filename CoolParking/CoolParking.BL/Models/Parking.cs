﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Models
{
    class Parking
    {
        private static Parking _parking;

        internal List<Vehicle> vehicles = new List<Vehicle>();

        public List<TransactionInfo> TransactionInfo { get; set; } = new List<TransactionInfo>();

        public int Capacity { get; private set; }

        public decimal Balance { get; internal set; }

        private Parking()
        {
            Capacity = Settings.Capacity;
            Balance = Settings.Balance;
        }

        public static Parking GetInstance()
        {
            return _parking ?? (_parking = new Parking());
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicles.Count >= Capacity)
            {
                throw new InvalidOperationException();
            }
            bool exist = vehicles.Any(x => x.Id == vehicle.Id);
            if (exist)
            {
                throw new ArgumentException();
            }
            vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicleToRemove = vehicles.FirstOrDefault(vehicle => vehicle.Id == vehicleId);
            if (vehicleToRemove == null)
            {
                throw new ArgumentException();
            }
            if (vehicleToRemove.Balance < 0)
            {
                throw new InvalidOperationException();
            }
            vehicles.Remove(vehicleToRemove);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicleToUp = vehicles.FirstOrDefault(vehicle => vehicle.Id == vehicleId);
            if (vehicleToUp == null || sum < 0)
            {
                throw new ArgumentException();
            }
            vehicleToUp.Balance += sum;
        }

        public int GetFreePlaces()
        {
            return Capacity - vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(vehicles);
        }

        public void GetPayment(object source, ElapsedEventArgs e)
        {
            decimal tariff;
            decimal diff;
            foreach (Vehicle vehicle in vehicles)
            {
                tariff = Settings.Tariff[vehicle.VehicleType];
                if (vehicle.Balance < 0)
                {
                    vehicle.Balance -= tariff * Settings.Penalty;
                    Balance += tariff * Settings.Penalty;
                }
                else if (vehicle.Balance < tariff)
                {
                    diff = tariff - vehicle.Balance;
                    Balance += vehicle.Balance;
                    vehicle.Balance = 0;
                    vehicle.Balance -= diff * Settings.Penalty;
                    Balance += diff * Settings.Penalty;
                }
                else
                {
                    vehicle.Balance -= tariff;
                    Balance += tariff;
                }
                TransactionInfo transaction = new TransactionInfo();
                transaction.Id = vehicle.Id;
                transaction.Time = DateTime.Now;
                transaction.Sum = tariff;
                TransactionInfo.Add(transaction);
            }
        }
    }
}