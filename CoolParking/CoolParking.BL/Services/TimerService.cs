﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private static Timer _timer;

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public TimerService(double interval)
        {
            Interval = interval;
            Interval *= 1000;
            _timer = new Timer(Interval)
            {
                AutoReset = true,
                Enabled = true
            };
            _timer.Elapsed += RaiseElapsed;
        }

        public void Dispose()
        {
            _timer.Dispose();
        }       

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        private void RaiseElapsed(object source, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(source, e);
        }
    }
}