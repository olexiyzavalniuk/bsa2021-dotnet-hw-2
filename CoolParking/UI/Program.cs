﻿using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System.IO;
using System.Reflection;

namespace UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("-------------Welcome to Cool-Parking--------------");
            Console.WriteLine("--------------------------------------------------");
            TimerService payTimer = new TimerService(Settings.PaymentTime);
            TimerService logTimer = new TimerService(Settings.LogTime);
            ParkingService parkingService = null;

            bool f = false;
            while (!f)
            {
                string path = "";
                Console.WriteLine();
                Console.WriteLine("First you must enter directory for log-file");
                Console.WriteLine();
                Console.Write("Path: ");
                path = Console.ReadLine() + "\\Transactions.log";
                LogService logService = new LogService(path);
                parkingService = new ParkingService(payTimer, logTimer, logService);
                f = Chekk(parkingService);
                if (f == false)
                {
                    Console.WriteLine();
                    Console.WriteLine("Bad path");
                }
            }

            Console.WriteLine();
            Console.WriteLine(" This is console interface. You can use command ");
            Console.WriteLine(" \"man\" to see list of available commands. And ");
            Console.WriteLine(" \"quit\" to exit from program ");
            Console.WriteLine();


            bool flag = true;
            while (flag)
            {
                Console.Write(">");
                string command = Console.ReadLine();
                Console.WriteLine();
                switch (command)
                {
                    case "quit":
                        flag = false;
                        break;
                    case "man":
                        Man();
                        break;
                    case "balance":
                        Balance(parkingService);
                        break;
                    case "sum":
                        Sum(parkingService);
                        break;
                    case "free":
                        Free(parkingService);
                        break;
                    case "curntr":
                        CurrentTransactions(parkingService);
                        break;
                    case "history":
                        History(parkingService);
                        break;
                    case "vehicles":
                        AllVehicles(parkingService);
                        break;
                    case "put":
                        Put(parkingService);
                        break;
                    case "take":
                        Take(parkingService);
                        break;
                    case "pay":
                        Replenish(parkingService);
                        break;
                    default:
                        Console.WriteLine("What? =/");
                        break;
                }
                Console.WriteLine();
            }

        }



        static void Man()
        {
            Console.WriteLine("quit - exit from program");
            Console.WriteLine("balance - balance of parking");
            Console.WriteLine("sum - sum of current earnings");
            Console.WriteLine("free - free/busy places on parking");
            Console.WriteLine("curntr - current transactions");
            Console.WriteLine("history - history of transactions");
            Console.WriteLine("vehicles - list of all vehicles on parking");
            Console.WriteLine("put - put vehicle on parking");
            Console.WriteLine("take - take vehicle from parking");
            Console.WriteLine("pay - replenish vehicle balance");
        }

        static void Balance(ParkingService parkingService)
        {
            decimal balance = parkingService.GetBalance();
            Console.WriteLine("Balance of parking: {0}", balance);
        }

        static void Sum(ParkingService parkingService)
        {
            decimal sum = parkingService.Sum();
            Console.WriteLine("Sum of current earnings: {0}", sum);
        }

        static void Free(ParkingService parkingService)
        {
            int free = parkingService.GetFreePlaces();
            int capacity = parkingService.GetCapacity();
            Console.WriteLine("Free {0} places from {1} (busy {2})", free, capacity, capacity - free);
        }

        static void CurrentTransactions(ParkingService parkingService)
        {
            string[] str = parkingService.LogForUI();
            foreach (string s in str)
            {
                Console.WriteLine(s);
            }
        }
        static void History(ParkingService parkingService)
        {
            try
            {
                Console.WriteLine(parkingService.ReadFromLog());
            }
            catch (Exception)
            {
                Console.WriteLine("Log does not exist");
            }
        }

        static void AllVehicles(ParkingService parkingService)
        {
            string[] str = parkingService.AllVihicles();
            foreach (string s in str)
            {
                Console.WriteLine(s);
            }
        }

        static void Put(ParkingService parkingService)
        {
            try
            {
                Console.WriteLine("First you have to say what you want to put");
                Console.WriteLine();
                Console.WriteLine("Enter ID (For example AA-1234-BB)");
                Console.WriteLine();
                Console.Write("ID: ");
                string id = Console.ReadLine();
                Console.WriteLine();
                Console.WriteLine("Enter number to set vehicle type:");
                Console.WriteLine("1 - car  2 - bus  3 - truck  4 - motorcycle ");
                Console.Write("Type: ");
                int type = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                Console.WriteLine("Enter bigin vehicle balance");
                Console.Write("Balance: ");
                decimal balance = Convert.ToDecimal(Console.ReadLine());
                Console.WriteLine();
                Vehicle vehicle;
                switch (type)
                {
                    case 1:
                        vehicle = new Vehicle(id, VehicleType.PassengerCar, balance);
                        parkingService.AddVehicle(vehicle);
                        break;
                    case 2:
                        vehicle = new Vehicle(id, VehicleType.Bus, balance);
                        parkingService.AddVehicle(vehicle);
                        break;
                    case 3:
                        vehicle = new Vehicle(id, VehicleType.Truck, balance);
                        parkingService.AddVehicle(vehicle);
                        break;
                    case 4:
                        vehicle = new Vehicle(id, VehicleType.Motorcycle, balance);
                        parkingService.AddVehicle(vehicle);
                        break;
                }
                Console.WriteLine("OK");
            }
            catch (Exception)
            {
                Console.WriteLine();
                Console.WriteLine("Dude, you entered something incorrectly!");
                Console.WriteLine("Try Again!");
            }
        }

        static void Take(ParkingService parkingService)
        {
            Console.WriteLine("Enter vehicle ID wich you wanna take");
            Console.WriteLine();
            Console.Write("ID: ");
            try
            {
                string id = Console.ReadLine();
                parkingService.RemoveVehicle(id);
                Console.WriteLine("OK");
            }
            catch (Exception)
            {
                Console.WriteLine();
                Console.WriteLine("Dude, you entered something incorrectly!");
                Console.WriteLine("Try Again!");
            }
        }

        static void Replenish(ParkingService parkingService)
        {
            Console.WriteLine("Enter vehicle ID its balance you wanna replenish");
            Console.WriteLine();
            Console.Write("ID: ");
            try
            {
                string id = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Sum: ");
                decimal sum = Convert.ToDecimal(Console.ReadLine());
                parkingService.TopUpVehicle(id, sum);
                Console.WriteLine("OK");
            }
            catch (Exception)
            {
                Console.WriteLine();
                Console.WriteLine("Dude, you entered something incorrectly!");
                Console.WriteLine("Try Again!");
            }
        }
        static bool Chekk(ParkingService parkingService)
        {
            try
            {
                _ = parkingService.ReadFromLog();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
